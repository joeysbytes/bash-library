# The ANSI BASH Library

## Description

This library provides global variables which can be used in your output to provide colors and attributes for your
text. 

These variables can be "turned off and on" as needed, which allows you to keep the ANSI codes in your program code.
IE, you don't have to write if statements to modify your output if you want to turn the ANSI codes off and on.

## Examples

To output the ANSI codes with the *echo* command, you need to add the -e flag.

```bash
source lib_loader.bash   # assuming you are in the bash project directory
lib_loader::load "ansi"  # load the ANSI library
ansi::on                 # by default, ANSI codes are "off", you need to turn them on

# Outputs a colorful message
echo -e "${fgYellow2}${bgBlue2}${blinkOn}Congratulations!${blinkOff}${fgDefault}${bgDefault}"

ansi::off                # set all ANSI variables to empty strings

# Outputs a plain text message
echo -e "${fgYellow2}${bgBlue2}${blinkOn}Congratulations!${blinkOff}${fgDefault}${bgDefault}"

echo -e "${resetAnsi}"   # reset all ANSI sequences before end of script
```

## Variables and Usage

### esc & csi

You do not need to use these values. They are used internally by the other ANSI escape sequences, but are
available if you wish to use them.

### resetAnsi

This is a full terminal reset of any ANSI codes which may be enabled. All colors will be set back to the terminal
defaults, and all attributes will be turned off.

This is very useful as the very last output in a script, before returning to the normal bash prompt.

### Colors

There are 8 defined ANSI colors, with a normal and a bright version, for a total of 16 colors. You set the
foreground and background colors separately. They are designed to be treated like tags within your strings.

The special variables *"fgDefault"* and *"bgDefault"* are used to turn off the currently set foreground and 
background colors, respectively. This will set your colors back to the defaults of your terminal.

#### Color Variables Table

* Prefix fg = foreground
* Prefix bg = background
* Suffix 2 = bright

| Normal Foreground Color | Bright Foreground Color | Foreground Color Off | Normal Background Color | Bright Background Color | Background Color Off |
|-------------------------|-------------------------|----------------------|-------------------------|-------------------------|----------------------|
| fgBlack                 | fgBlack2                | fgDefault            | bgBlack                 | bgBlack2                | bgDefault            |
| fgBlue                  | fgBlue2                 | fgDefault            | bgBlue                  | bgBlue2                 | bgDefault            |
| fgCyan                  | fgCyan2                 | fgDefault            | bgCyan                  | bgCyan2                 | bgDefault            |
| fgGreen                 | fgGreen2                | fgDefault            | bgGreen                 | bgGreen2                | bgDefault            |
| fgMagenta               | fgMagenta2              | fgDefault            | bgMagenta               | bgMagenta2              | bgDefault            |
| fgRed                   | fgRed2                  | fgDefault            | bgRed                   | bgRed2                  | bgDefault            |
| fgWhite                 | fgWhite2                | fgDefault            | bgWhite                 | bgWhite2                | bgDefault            |
| fgYellow                | fgYellow2               | fgDefault            | bgYellow                | bgYellow2               | bgDefault            |

### Attributes

#### Intensity Attributes

You can set how "bright" your text appears on the screen to 1 of 3 intensity levels by outputting one of these 
variables. This is separate from setting a normal or bright color.

To reset the intensity level, just output the *"intensity1"* value.

| Intensity Variable | Description               |
|--------------------|---------------------------|
| intensity0         | Dim                       |
| intensity1         | Medium / Normal / Default |
| intensity2         | Bright                    |

#### Text Decoration Attributes

These variables allow you to decorate your text.

| Attribute On    | Attribute Off    |
|-----------------|------------------|
| blinkOn         | blinkOff         |
| italicOn        | italicOff        |
| reverseOn       | reverseOff       |
| strikethroughOn | strikethroughOff |
| underlineOn     | underlineOff     |
