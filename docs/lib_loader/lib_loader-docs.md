# lib_loader

## Description

The *lib_loader* is responsible for managing the loading of all the other bash libraries, as well as their
dependencies. You source this library in first, and then use its functions to load and initialize the other libraries.

It also provides the global variable *BASH_FUNCTION_RETURN_CODE*. Most of the bash library functions use this
to set their status codes, rather than using return codes.

## Functions

While a bash library may have many functions, only the ones you are expected to call as a user will be
documented here. Any undocumented function is for internal use. However, if you are really curious, the functions
will be well documented within the source code.

### lib_loader::load

| Topic            | Values                                                                                                                                                                                                                                                               |
|------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Description**  | Given a library name, load it (if not already loaded), and run its initialization function.                                                                                                                                                                          |
| **Parameters**   | 1 - Library name                                                                                                                                                                                                                                                     |
| **Status Codes** | 0 - Library successfully loaded<br />1 - Library already loaded<br />128 - Invalid number of arguments<br />129 - Library not found<br />253 - Error occurred in library initialization<br />254 - Failed to source in library<br />255 - Unspecified error occurred |

### lib_loader::is_loaded

| Topic            | Values                                                                                |
|------------------|---------------------------------------------------------------------------------------|
| **Description**  | Given a library name, set the status code indicating if the library has been loaded.  |                                                                                                                                                                           
| **Parameters**   | 1 - Library name                                                                      |
| **Status Codes** | 0 - Library loaded<br />1 - Library not loaded<br />128 - Invalid number of arguments |
