from bash_runner import *


###########################################################################
# lib_loader::load_available_libs
###########################################################################

def test_load_available_libs_no_args():
    expected = {"stdout": "",
                "stderr": "USAGE: lib_loader::load_available_libs libraryDirectory\n",
                "rc": 128}
    cmd = 'lib_loader::load_available_libs'
    validate_bash_results(expected, run_bash_test(cmd))


def test_load_available_libs_too_many_args():
    expected = {"stdout": "",
                "stderr": "USAGE: lib_loader::load_available_libs libraryDirectory\n",
                "rc": 128}
    cmd = 'lib_loader::load_available_libs "a" "b"'
    validate_bash_results(expected, run_bash_test(cmd))


def test_load_available_libs_dir_not_found():
    test_dir = "/zzz_dirNotExists_zzz"
    expected = {"stdout": "",
                "stderr": f"ERROR: lib_loader::load_available_libs: Library directory not found: {test_dir}\n",
                "rc": 129}
    cmd = f'lib_loader::load_available_libs "{test_dir}"'
    validate_bash_results(expected, run_bash_test(cmd))


def test_load_available_libs_empty_dir(tmp_path):
    expected = {"stdout": None,
                "stderr": "",
                "rc": 0}
    cmd = 'echo -n "${#_lib_loader_status[@]},${#_lib_loader_paths[@]} "'
    cmd += f';lib_loader::load_available_libs "{tmp_path}"'
    cmd += ';echo -n "${#_lib_loader_status[@]},${#_lib_loader_paths[@]}"'
    result = run_bash_test(cmd)
    validate_bash_results(expected, result)
    pre_status_value = result["stdout"].split(" ")[0].split(",")[0]
    post_status_value = result["stdout"].split(" ")[1].split(",")[0]
    assert pre_status_value == post_status_value
    pre_paths_value = result["stdout"].split(" ")[0].split(",")[1]
    post_paths_value = result["stdout"].split(" ")[1].split(",")[1]
    assert pre_paths_value == post_paths_value


def test_load_available_libs_three_dirs(tmp_path):
    # create 3 directories
    for i in range(1, 4, 1):
        d = tmp_path / f"dir_{i}"
        d.mkdir()
    # the actual test
    expected = {"stdout": None,
                "stderr": "",
                "rc": 0}
    cmd = 'echo -n "${#_lib_loader_status[@]},${#_lib_loader_paths[@]} "'
    cmd += f';lib_loader::load_available_libs "{tmp_path}"'
    cmd += ';echo -n "${#_lib_loader_status[@]},${#_lib_loader_paths[@]}"'
    result = run_bash_test(cmd)
    validate_bash_results(expected, result)
    pre_status_value = result["stdout"].split(" ")[0].split(",")[0]
    post_status_value = result["stdout"].split(" ")[1].split(",")[0]
    assert int(pre_status_value) + 3 == int(post_status_value)
    pre_paths_value = result["stdout"].split(" ")[0].split(",")[1]
    post_paths_value = result["stdout"].split(" ")[1].split(",")[1]
    assert int(pre_paths_value) + 3 == int(post_paths_value)

# test lib status values
# test lib already loaded, not overwriting values

###########################################################################
# lib_loader::lib_status
###########################################################################

def test_lib_status_no_args():
    expected = {"stdout": "",
                "stderr": "USAGE: lib_loader::lib_status libraryName\n",
                "rc": 128}
    cmd = 'lib_loader::lib_status'
    validate_bash_results(expected, run_bash_test(cmd))


def test_lib_status_too_many_args():
    expected = {"stdout": "",
                "stderr": "USAGE: lib_loader::lib_status libraryName\n",
                "rc": 128}
    cmd = 'lib_loader::lib_status "a" "b"'
    validate_bash_results(expected, run_bash_test(cmd))


def test_lib_status_not_found():
    expected = {"stdout": "not found",
                "stderr": "",
                "rc": 0}
    cmd = 'lib_loader::lib_status "zzz_dummy_zzz"'
    validate_bash_results(expected, run_bash_test(cmd))



# def test_is_loaded_not_loaded():
#     expected = {"stdout": "not loaded",
#                 "stderr": "",
#                 "rc": 0}
#     cmd = 'lib_loader::is_loaded "dummy_good"'
#     validate_bash_results(expected, run_bash_test(cmd))
#
#
# def test_is_loaded_already_loaded():
#     expected = {"stdout": "loaded",
#                 "stderr": "",
#                 "rc": 0}
#     cmd = 'lib_loader::is_loaded "lib_loader"'
#     validate_bash_results(expected, run_bash_test(cmd))


###########################################################################
# lib_loader::initialize
###########################################################################

# def test_initialize_twice():
#     expected = {"stdout": "loaded",
#                 "stderr": "",
#                 "rc": 0}
#     cmd = 'lib_loader::initialize'
#     cmd += ';lib_loader::is_loaded "lib_loader"'
#     validate_bash_results(expected, run_bash_test(cmd))


###########################################################################
# lib_loader::load
###########################################################################

# def test_load_no_args():
#     expected = {"stdout": "",
#                 "stderr": "USAGE: lib_loader::load libraryName\n",
#                 "rc": 128}
#     cmd = 'lib_loader::load'
#     validate_bash_results(expected, run_bash_test(cmd))
#
#
# def test_load_too_many_args():
#     expected = {"stdout": "",
#                 "stderr": "USAGE: lib_loader::load libraryName\n",
#                 "rc": 128}
#     cmd = 'lib_loader::load "a" "b"'
#     validate_bash_results(expected, run_bash_test(cmd))
#
#
# def test_load_already_loaded():
#     expected = {"stdout": "",
#                 "stderr": "",
#                 "rc": 0}
#     cmd = 'lib_loader::load "lib_loader"'
#     validate_bash_results(expected, run_bash_test(cmd))
#
#
# def test_load_success():
#     expected = {"stdout": "",
#                 "stderr": "",
#                 "rc": 0}
#     cmd = 'lib_loader::load "dummy_good"'
#     validate_bash_results(expected, run_bash_test(cmd))
#
#
# def test_load_failed():
#     expected = {"stdout": "",
#                 "stderr": "ERROR: Initialization error occurred in library: dummy_bad\n",
#                 "rc": 253}
#     cmd = 'lib_loader::load "dummy_bad"'
#     validate_bash_results(expected, run_bash_test(cmd, disable_error_check=True))
#
#
# def test_load_lib_not_found():
#     expected = {"stdout": "",
#                 "stderr": "ERROR: Could not find library to load: zzz_dummy_zzz\n",
#                 "rc": 129}
#     cmd = 'lib_loader::load "zzz_dummy_zzz"'
#     validate_bash_results(expected, run_bash_test(cmd))
