###########################################################################
# A wrapper to run bash commands from the Python unit tests.
#
# Globals:
#   None
#
# Arguments:
#   1) Command(s) to execute
#
# Outputs:
#   None
###########################################################################
set -e

# Verify inputs
if [ $# -ne 1 ]; then
    echo "USAGE: bash-runner.bash commandToExecute">&2
    exit 1
fi
__eval_status_code=-1
__command_to_execute="${1}"


# We always need to start with sourcing in the lib_loader
__lib_loader_file="../src/lib_loader.bash"
if [ ! -f "${__lib_loader_file}" ]; then
    echo "ERROR: lib_loader file not found: ${__lib_loader_file}">&2
    exit 255
fi
# shellcheck source=../src/lib_loader.bash
source "${__lib_loader_file}"


# Run the test command(s)
eval "${__command_to_execute}"
__eval_status_code=$?


# Return the same status code as returned from the __command_to_execute
exit "${__eval_status_code}"
