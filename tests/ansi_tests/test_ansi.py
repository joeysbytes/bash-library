from bash_runner import *
import pytest

ESC = "\033"
CSI = f"{ESC}["

ANSI_VARIABLES = [
    {"name": "esc", "value_on": ESC, "value_off": ""},
    {"name": "csi", "value_on": CSI, "value_off": ""},
    {"name": "resetAnsi", "value_on": f"{CSI}0m", "value_off": ""},
    {"name": "fgBlack", "value_on": f"{CSI}30m", "value_off": ""},
    {"name": "bgBlack", "value_on": f"{CSI}40m", "value_off": ""},
    {"name": "fgBlack2", "value_on": f"{CSI}90m", "value_off": ""},
    {"name": "bgBlack2", "value_on": f"{CSI}100m", "value_off": ""},
    {"name": "fgRed", "value_on": f"{CSI}31m", "value_off": ""},
    {"name": "bgRed", "value_on": f"{CSI}41m", "value_off": ""},
    {"name": "fgRed2", "value_on": f"{CSI}91m", "value_off": ""},
    {"name": "bgRed2", "value_on": f"{CSI}101m", "value_off": ""},
    {"name": "fgGreen", "value_on": f"{CSI}32m", "value_off": ""},
    {"name": "bgGreen", "value_on": f"{CSI}42m", "value_off": ""},
    {"name": "fgGreen2", "value_on": f"{CSI}92m", "value_off": ""},
    {"name": "bgGreen2", "value_on": f"{CSI}102m", "value_off": ""},
    {"name": "fgYellow", "value_on": f"{CSI}33m", "value_off": ""},
    {"name": "bgYellow", "value_on": f"{CSI}43m", "value_off": ""},
    {"name": "fgYellow2", "value_on": f"{CSI}93m", "value_off": ""},
    {"name": "bgYellow2", "value_on": f"{CSI}103m", "value_off": ""},
    {"name": "fgBlue", "value_on": f"{CSI}34m", "value_off": ""},
    {"name": "bgBlue", "value_on": f"{CSI}44m", "value_off": ""},
    {"name": "fgBlue2", "value_on": f"{CSI}94m", "value_off": ""},
    {"name": "bgBlue2", "value_on": f"{CSI}104m", "value_off": ""},
    {"name": "fgMagenta", "value_on": f"{CSI}35m", "value_off": ""},
    {"name": "bgMagenta", "value_on": f"{CSI}45m", "value_off": ""},
    {"name": "fgMagenta2", "value_on": f"{CSI}95m", "value_off": ""},
    {"name": "bgMagenta2", "value_on": f"{CSI}105m", "value_off": ""},
    {"name": "fgCyan", "value_on": f"{CSI}36m", "value_off": ""},
    {"name": "bgCyan", "value_on": f"{CSI}46m", "value_off": ""},
    {"name": "fgCyan2", "value_on": f"{CSI}96m", "value_off": ""},
    {"name": "bgCyan2", "value_on": f"{CSI}106m", "value_off": ""},
    {"name": "fgWhite", "value_on": f"{CSI}37m", "value_off": ""},
    {"name": "bgWhite", "value_on": f"{CSI}47m", "value_off": ""},
    {"name": "fgWhite2", "value_on": f"{CSI}97m", "value_off": ""},
    {"name": "bgWhite2", "value_on": f"{CSI}107m", "value_off": ""},
    {"name": "fgDefault", "value_on": f"{CSI}39m", "value_off": ""},
    {"name": "bgDefault", "value_on": f"{CSI}49m", "value_off": ""},
    {"name": "intensity1", "value_on": f"{CSI}22m", "value_off": ""},
    {"name": "intensity2", "value_on": f"{CSI}22m{CSI}1m", "value_off": ""},
    {"name": "intensity0", "value_on": f"{CSI}22m{CSI}2m", "value_off": ""},
    {"name": "underlineOn", "value_on": f"{CSI}4m", "value_off": ""},
    {"name": "underlineOff", "value_on": f"{CSI}24m", "value_off": ""},
    {"name": "italicOn", "value_on": f"{CSI}3m", "value_off": ""},
    {"name": "italicOff", "value_on": f"{CSI}23m", "value_off": ""},
    {"name": "blinkOn", "value_on": f"{CSI}5m", "value_off": ""},
    {"name": "blinkOff", "value_on": f"{CSI}25m", "value_off": ""},
    {"name": "reverseOn", "value_on": f"{CSI}7m", "value_off": ""},
    {"name": "reverseOff", "value_on": f"{CSI}27m", "value_off": ""},
    {"name": "strikethroughOn", "value_on": f"{CSI}9m", "value_off": ""},
    {"name": "strikethroughOff", "value_on": f"{CSI}29m", "value_off": ""}
]


@pytest.mark.parametrize("ansi_dict", ANSI_VARIABLES)
def test_ansi_values_on(ansi_dict):
    expected = {"stdout": ansi_dict["value_on"],
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "ansi"'
    command += ';ansi::on'
    command += ';echo -e -n "${' + ansi_dict["name"] + '}"'
    validate_bash_results(expected, run_bash_test(command))


@pytest.mark.parametrize("ansi_dict", ANSI_VARIABLES)
def test_ansi_values_off(ansi_dict):
    expected = {"stdout": ansi_dict["value_off"],
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "ansi"'
    command += ';ansi::on'
    command += ';ansi::off'
    command += ';echo -e -n "${' + ansi_dict["name"] + '}"'
    validate_bash_results(expected, run_bash_test(command))


@pytest.mark.parametrize("ansi_dict", ANSI_VARIABLES)
def test_ansi_values_default(ansi_dict):
    expected = {"stdout": ansi_dict["value_off"],
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "ansi"'
    command += ';echo -e -n "${' + ansi_dict["name"] + '}"'
    validate_bash_results(expected, run_bash_test(command))


def test_ansi_on():
    expected = {"stdout": "0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "ansi"'
    command += ';ansi::on'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'


def test_ansi_off():
    expected = {"stdout": "0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "ansi"'
    command += ';ansi::off'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
