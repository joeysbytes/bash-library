import subprocess
from typing import Dict


def run_bash_test(command: str, disable_error_check=False) -> Dict:
    """Given a command to run, execute and return the results.
    Parameters:
        command: The bash command to run
        disable_error_check: set to True if multiple bash steps will have rc > 0
    Returns a dictionary with the following keys:
        stdout: The captured stdout
        stderr: The captured stderr
        rc: The return code of the command
    """
    # if the expected rc != 0, then we want to turn error exiting off while running the command
    if disable_error_check:
        command = 'set +e;' + command

    # set up command
    cmd = ["/usr/bin/env",
           "bash",
           "./_bash-runner/bash-runner.bash",
           command]
    # run the command
    status = subprocess.run(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            check=False,
                            shell=False)
    # build results dictionary
    results = {"rc": status.returncode,
               "stdout": status.stdout.decode('UTF-8'),
               "stderr": status.stderr.decode('UTF-8')}
    # Return the results
    return results


def validate_bash_results(expected: Dict, actual: Dict) -> None:
    """Given the results of a previous run, compare the values to that of the expected values
    in the same dictionary format. If the expected value is set to None, it will not be checked."""
    if expected["rc"] is not None:
        assert expected["rc"] == actual["rc"]
    if expected["stdout"] is not None:
        assert expected["stdout"] == actual["stdout"]
    if expected["stderr"] is not None:
        assert expected["stderr"] == actual["stderr"]
