from logger_tests.logger_variables import *
from bash_runner import *
import pytest


###########################################################################
# initialization
###########################################################################

@pytest.mark.parametrize(
    "variable_name,value",
    [("_LOGGER_LEVEL", "30"),
     ("_LOGGER_LEVEL_ENABLED", "1"),
     ("_LOGGER_TIMESTAMP_ENABLED", "1"),
     ("_LOGGER_TIMESTAMP_FORMAT", DEFAULT_TIMESTAMP),
     ("BASH_FUNCTION_RETURN_CODE", "0")]
)
def test_default_values(variable_name, value):
    expected = {"stdout": value,
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';echo -n "${' + variable_name + '}"'
    validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::enable_log_level
###########################################################################

def test_enable_log_level_already_enabled():
    expected = {"stdout": "1:1",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::enable_log_level'
    command += ';echo -n "${_LOGGER_LEVEL_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_enable_log_level_already_disabled():
    expected = {"stdout": "1:0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';_LOGGER_LEVEL_ENABLED=0'
    command += ';logger::enable_log_level'
    command += ';echo -n "${_LOGGER_LEVEL_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::disable_log_level
###########################################################################

def test_disable_log_level_already_enabled():
    expected = {"stdout": "0:0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::disable_log_level'
    command += ';echo -n "${_LOGGER_LEVEL_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_disable_log_level_already_disabled():
    expected = {"stdout": "0:1",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';_LOGGER_LEVEL_ENABLED=0'
    command += ';logger::disable_log_level'
    command += ';echo -n "${_LOGGER_LEVEL_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::set_log_level
###########################################################################

def test_set_log_level_no_args():
    expected = {"stdout": "128",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_log_level'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_set_log_level_too_many_args():
    expected = {"stdout": "128",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_log_level "a" "b"'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_set_log_level_invalid_value():
    expected = {"stdout": "129",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_log_level "zzz"'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


@pytest.mark.parametrize("logger_dict", LOGGER_VARIABLES)
def test_set_log_level_valid_value(logger_dict):
    if logger_dict["level"] is not None:
        expected = {"stdout": str(logger_dict["level"]),
                    "stderr": "",
                    "rc": 0}
        if logger_dict["level_name"] == "info":
            expected["stdout"] += ":1"
        else:
            expected["stdout"] += ":0"
        command = 'lib_loader::load "logger"'
        command += ';logger::set_log_level "' + logger_dict["level_name"] + '"'
        command += ';echo -n "${_LOGGER_LEVEL}:${BASH_FUNCTION_RETURN_CODE}"'
        validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::enable_timestamp
###########################################################################

def test_enable_timestamp_already_enabled():
    expected = {"stdout": "1:1",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::enable_timestamp'
    command += ';echo -n "${_LOGGER_TIMESTAMP_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_enable_timestamp_already_disabled():
    expected = {"stdout": "1:0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';_LOGGER_TIMESTAMP_ENABLED=0'
    command += ';logger::enable_timestamp'
    command += ';echo -n "${_LOGGER_TIMESTAMP_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::disable_timestamp
###########################################################################

def test_disable_timestamp_already_enabled():
    expected = {"stdout": "0:0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::disable_timestamp'
    command += ';echo -n "${_LOGGER_TIMESTAMP_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_disable_timestamp_already_disabled():
    expected = {"stdout": "0:1",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';_LOGGER_TIMESTAMP_ENABLED=0'
    command += ';logger::disable_timestamp'
    command += ';echo -n "${_LOGGER_TIMESTAMP_ENABLED}:${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


###########################################################################
# logger::set_timestamp_format
###########################################################################

def test_set_timestamp_format_no_args():
    expected = {"stdout": "128",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_timestamp_format'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_set_timestamp_format_too_many_args():
    expected = {"stdout": "128",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_timestamp_format "a" "b"'
    command += ';echo -n "${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_set_timestamp_format_no_change():
    expected = {"stdout": DEFAULT_TIMESTAMP + " 1",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_timestamp_format "' + DEFAULT_TIMESTAMP + '"'
    command += ';echo -n "${_LOGGER_TIMESTAMP_FORMAT} ${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))


def test_set_timestamp_format_changed():
    new_timestamp = "%Y%m%d_%H%M%S"
    expected = {"stdout": new_timestamp + " 0",
                "stderr": "",
                "rc": 0}
    command = 'lib_loader::load "logger"'
    command += ';logger::set_timestamp_format "' + new_timestamp + '"'
    command += ';echo -n "${_LOGGER_TIMESTAMP_FORMAT} ${BASH_FUNCTION_RETURN_CODE}"'
    validate_bash_results(expected, run_bash_test(command))
