from logger_tests.logger_variables import *
from bash_runner import *
import pytest


###########################################################################
# log
###########################################################################

@pytest.mark.parametrize("logger_dict", LOGGER_VARIABLES)
def test_log_off(logger_dict):
    if logger_dict["output"] is not None:
        log_msg = "test log message"
        expected = {"stdout": "",
                    "stderr": "",
                    "rc": 0}
        command = 'lib_loader::load "logger"'
        command += ';logger::set_log_level "off"'
        command += ';log '
        validate_bash_results(expected, run_bash_test(command))


# def test_log_no_args():
#     expected = {"stdout": "\n",
#                 "stderr": "",
#                 "rc": 0}
#     command = 'lib_loader::load "logger"'
#     command += ';log'
#     validate_bash_results(expected, run_bash_test(command))
