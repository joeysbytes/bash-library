LOGGER_VARIABLES = [
    {"level_name": "all", "level": 0, "output": None},
    {"level_name": "trace", "level": 10, "output": "TRACE"},
    {"level_name": "debug", "level": 20, "output": "DEBUG"},
    {"level_name": "info", "level": 30, "output": "INFO "},
    {"level_name": "warn", "level": 40, "output": "WARN "},
    {"level_name": "warning", "level": 40, "output": "WARN "},
    {"level_name": "err", "level": 50, "output": "ERROR"},
    {"level_name": "error", "level": 50, "output": "ERROR"},
    {"level_name": "fatal", "level": 60, "output": "FATAL"},
    {"level_name": "unknown", "level": None, "output": "?????"},
    {"level_name": "off", "level": 99, "output": None}
]

DEFAULT_TIMESTAMP = "%Y-%m-%d %H:%M:%S"
