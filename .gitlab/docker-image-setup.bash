#!/usr/bin/env bash
set -e


function main() {
    initialize
    install_apt_dependencies
    install_pip_dependencies
}


function initialize() {
    echo "Verifying we are in a Docker container"
    if [ ! -f "/.dockerenv" ]
    then
        echo "ERROR: Not inside Docker container">&2
        exit 1
    fi

    echo "Verifying we are root"
    local user
    user="$(whoami)"
    if [ ! "${user}" == "root" ]
    then
        echo "ERROR: Not root user: ${user}">&2
        exit 1
    fi

    echo "Verifying we are on an apt system"
    local is_apt_found
    set +e
    which apt-get
    is_apt_found=$?
    set -e
    if [ "${is_apt_found}" -ne 0 ]
    then
        echo "ERROR: apt-get not found">&2
        exit 1
    fi
}


function install_apt_dependencies() {
    export DEBIAN_FRONTEND=noninteractive
    echo "Updating apt cache"
    apt-get update
    echo "Installing shellcheck"
    apt-get install --yes shellcheck
    echo "Cleaning apt files"
    apt-get clean
    rm -rf /var/cache/apt/*
    rm -rf /var/lib/apt/lists/*
}


function install_pip_dependencies() {
    echo "Installing pytest"
    pip install --no-cache-dir pytest
    echo "Cleaning pip files"
    pip cache purge
    rm -rf "$(pip cache dir)"
}


main
