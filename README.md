# Joey's Bytes Infamous Bash Library

## The Library (/lib)

| Library                            | Description                                        | Documentation                                           | Demos                                    |
|------------------------------------|----------------------------------------------------|---------------------------------------------------------|------------------------------------------|
| [lib_loader](src/lib_loader.bash)  | Bash Library Loader / Manager (the starting point) | [lib_loader docs](./docs/lib_loader/lib_loader-docs.md) | n/a                                      |
| [ansi](src/standard-lib/ansi.bash) | ANSI Color and Attribute Global Variables          | [ansi docs](./docs/ansi/ansi-docs.md)                   | [ansi demo](./demos/ansi/ansi-demo.bash) |

## Requirements

To run these BASH library utilities, and demos, the only requirements will be to have BASH, and any commands
the utilities are calling, installed.

To run the unit tests, a recent version of Python 3 and PyTest are required.

## The Makefile

Running the "make" command without any parameters will display help text showing the available options. The Makefile
can be used to run unit tests and demos.
