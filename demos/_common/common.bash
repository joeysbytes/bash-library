# shellcheck disable=SC2154  # all ansi variables are externally sourced

function init() {
    # Source the library loader
    source "../../src/lib_loader.bash"
    lib_loader::load "ansi"
    ansi::on
    # Clear the screen
    default_color="${fgWhite}${bgBlack}"
    echo -e -n "${default_color}"
    clear
}


function finalize() {
    echo -e -n "${resetAnsi}"
    clear
}


function press_enter_to_continue() {
    echo -e -n "${fgBlack}${bgGreen2}Press Enter to continue...${default_color}"
    read -r
}
