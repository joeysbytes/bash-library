#!/usr/bin/env bash
# shellcheck disable=SC2154  # all ansi variables are externally sourced


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    initialize

    logger::enable_log_level
    logger::enable_timestamp
    show_log_lines "off"
    echo ""
    show_log_lines "on"
    echo ""

    logger::disable_log_level
    logger::enable_timestamp
    show_log_lines "off"
    echo ""
    show_log_lines "on"
    echo ""

    logger::enable_log_level
    logger::disable_timestamp
    show_log_lines "off"
    echo ""
    show_log_lines "on"
    echo ""

    logger::disable_log_level
    logger::disable_timestamp
    show_log_lines "off"
    echo ""
    show_log_lines "on"
    echo ""

    press_enter_to_continue
    finalize
}


function initialize() {
    # Load common functions
    source "../_common/common.bash"
    init
    # Load logger lib
    lib_loader::load "logger"
    logger::set_log_level "all"
}


###########################################################################
# Show sample of all log types
###########################################################################
function show_log_lines() {
    local ansi_status="${1}"
    ansi::on
    echo -e "${fgYellow2}${underlineOn}Showing log lines with ANSI ${ansi_status}${underlineOff}${fgDefault}"
    echo ""
    if [ "${ansi_status}" == "off" ]; then
        ansi::off
    fi
    logger::set_log_level "all"
    _show_log_line "trace"
    _show_log_line "debug"
    _show_log_line "info"
    _show_log_line "warn"
    _show_log_line "warning"
    _show_log_line "err"
    _show_log_line "error"
    _show_log_line "fatal"
    _show_log_line "zzz"
}


function _show_log_line() {
    local log_level_name="${1}"
    local log_msg="This is a log message passed with a level of: ${log_level_name}"
    log "${log_level_name}" "${log_msg}"
}


###########################################################################
# Start the demo
###########################################################################
main
