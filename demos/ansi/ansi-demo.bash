#!/usr/bin/env bash
# shellcheck disable=SC2154  # all ansi variables are externally sourced
set -e


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    initialize
    show_color_table
    echo ""
    show_intensities
    echo ""
    show_attributes
    echo ""
    press_enter_to_continue
    finalize
}


function initialize() {
    # Load common functions
    source "../_common/common.bash"
    init
}

###########################################################################
# Display The Color Table
###########################################################################
function show_color_table() {
    _show_color_table_title
    echo ""
    _show_color_table_column_headings
    _show_color_line "Black"
    _show_color_line "Blue"
    _show_color_line "Cyan"
    _show_color_line "Green"
    _show_color_line "Magenta"
    _show_color_line "Red"
    _show_color_line "White"
    _show_color_line "Yellow"
}


function _show_color_table_title() {
    echo -e "${fgYellow2}ANSI Color Table "
}


function _show_color_table_column_headings() {
    echo -e "${fgCyan}   Normal        Bright        Normal        Bright${default_color}"
    echo -e "${fgCyan} Foreground    Foreground    Background    Background${default_color}"
    echo -e "${fgWhite}------------  ------------  ------------  ------------${default_color}"
}


function _show_color_line() {
    local line=" "
    local color="${1}"
    local color_len="${#color}"
    local padding=$((7-color_len))
    # Column 1 - Original Color
    local color_name="fg${color}"
    line="${line}${!color_name}"
    if [ "${color_name}" == "fgBlack" ]; then line="${line}${bgBlack2}"; fi
    line="${line}${color_name}"
    for ((i=0; i<"${padding}"; i++)); do line="${line} "; done
    line="${line}${default_color}     "
    # Column 2 - Bright Color
    color_name="fg${color}2"
    line="${line}${!color_name}"
    line="${line}${color_name}"
    for ((i=0; i<"${padding}"; i++)); do line="${line} "; done
    line="${line}${default_color}    "
    # Column 3 - Original Background Color
    color_name="bg${color}"
    line="${line}${!color_name}"
    if [ "${color_name}" == "bgBlack" ]; then line="${line}${fgBlack2}"; else line="${line}${fgBlack}"; fi
    line="${line}${color_name}"
    for ((i=0; i<"${padding}"; i++)); do line="${line} "; done
    line="${line}${default_color}     "
    # Column 4 - Bright Background Color
    color_name="bg${color}2"
    line="${line}${!color_name}"
    line="${line}${fgBlack}"
    line="${line}${color_name}"
    for ((i=0; i<"${padding}"; i++)); do line="${line} "; done
    # Output the line
    line="${line}${default_color}"
    echo -e "${line}"
}


###########################################################################
# Display Font Intensities
###########################################################################
function show_intensities() {
    echo -e "${fgYellow2}Intensities:${default_color}  ${intensity0}intensity0 - dim"
    echo -e "              ${intensity1}intensity1 - default / medium / normal"
    echo -e "              ${intensity2}intensity2 - bright${intensity1}"
}


###########################################################################
# Display Font Attributes
###########################################################################
function show_attributes() {
    echo -e -n "${fgYellow2}Attributes:${default_color}   "
    _show_attribute_line "blink" "no"
    _show_attribute_line "italic" "yes"
    _show_attribute_line "reverse" "yes"
    _show_attribute_line "strikethrough" "yes"
    _show_attribute_line "underline" "yes"
}


function _show_attribute_line() {
    local attribute="${1}"
    local attribute_on="${attribute}On"
    local ident="${2}"
    local attribute_off="${attribute}Off"
    local attribute_len="${#attribute}"
    local padding=$((13-attribute_len))
    # Column 1
    local line=""
    if [ "${ident}" == "yes" ]; then line="${line}              "; fi
    line="${line}${!attribute_on}"
    line="${line}${attribute_on}"
    for ((i=0; i<"${padding}"; i++)); do line="${line} "; done
    line="${line}${!attribute_off}  "
    # Column 2
    line="${line}${attribute_off}"
    echo -e "${line}"
}


###########################################################################
# Start The Demo
###########################################################################
main
