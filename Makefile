###########################################################################
# Help
###########################################################################
.PHONY: help
help:
	@echo "Available Make Targets"
	@echo ""
	@echo "Library               Run Tests             Run Demos"
	@echo "--------------------  --------------------  --------------------"
	@echo "lib_loader            lib_loader-tests      n/a"
	@echo "ansi                  ansi-tests            ansi-demo"
	@echo "logger                logger-tests          logger-demo"
	@echo ""
	@echo "Other Targets:"
	@echo "  all-tests: Run all tests"
	@echo "  clean:     Cleanup all cache / temp files, etc."


###########################################################################
# Mass Run Jobs
###########################################################################
.PHONY: all-tests
all-tests: copy_dummy_libs all-unit-tests all-shellcheck-tests clean

.PHONY: all-unit-tests
all-unit-tests:
	@echo "Running all unit tests"
	@cd tests \
		&& python -m pytest .

.PHONY: all-shellcheck-tests
all-shellcheck-tests:
	@echo "Running all shellcheck tests"
	@cd src \
		&& find . -name '*.bash' -exec shellcheck {} \;


###########################################################################
# lib_loader
###########################################################################
.PHONY: lib_loader-tests
lib_loader-tests: lib_loader-unit-tests lib_loader-shellcheck-tests clean

.PHONY: lib_loader-unit-tests
lib_loader-unit-tests: copy_dummy_libs _lib_loader-unit-tests clean_dummy_libs

.PHONY: _lib_loader-unit-tests
_lib_loader-unit-tests:
	@echo "Running unit tests: lib_loader"
	@cd tests \
		&& python -m pytest ./lib_loader_tests/

.PHONY: lib_loader-shellcheck-tests
lib_loader-shellcheck-tests:
	@echo "Running shellcheck: lib_loader"
	@cd src/standard-lib/lib_loader \
		&& find . -name '*.bash' -exec shellcheck {} \;

.PHONY: copy_dummy_libs
copy_dummy_libs:
	@echo "Copying dummy libs to extras" \
		&& cd tests/_bash_test_libs \
		&& cp --recursive dummy_good "../../src/extras-lib/" \
		&& cp --recursive dummy_bad "../../src/extras-lib/"


###########################################################################
# ansi
###########################################################################
.PHONY: ansi-tests
ansi-tests: ansi-unit-tests ansi-shellcheck-tests clean

.PHONY: ansi-unit-tests
ansi-unit-tests:
	@echo "Running unit tests: ansi"
	@cd tests \
		&& python -m pytest ./ansi_tests/

.PHONY: ansi-shellcheck-tests
ansi-shellcheck-tests:
	@echo "Running shellcheck: ansi"
	@cd src/standard-lib \
		&& shellcheck ansi.bash
	@echo "Running shellcheck: ansi demo"
	@cd demos/ansi \
		&& shellcheck --external-sources ansi-demo.bash

.PHONY: ansi-demo
ansi-demo:
	@cd "demos/ansi" \
		&& chmod +x "ansi-demo.bash" \
		&& ./ansi-demo.bash


###########################################################################
# logger
###########################################################################
.PHONY: logger-tests
logger-tests: logger-unit-tests logger-shellcheck-tests clean

.PHONY: logger-unit-tests
logger-unit-tests:
	@echo "Running unit tests: logger"
	@cd tests \
		&& python -m pytest ./logger_tests/

.PHONY: logger-shellcheck-tests
logger-shellcheck-tests:
	@echo "Running shellcheck: logger"
	@cd src/standard-lib \
		&& shellcheck logger.bash
	@echo "Running shellcheck: logger demo"
	@cd demos/logger \
		&& shellcheck --external-sources logger-demo.bash

.PHONY: logger-demo
logger-demo:
	@cd "demos/logger" \
		&& chmod +x "logger-demo.bash" \
		&& ./logger-demo.bash


###########################################################################
# Cleanup Utilities
###########################################################################
.PHONY: clean
clean: clean-pytest clean_dummy_libs

.PHONY: clean-pytest
clean-pytest:
	@echo "Cleaning PyTest files"
	@cd tests \
		&& if [ -d ".pytest_cache" ]; then rm -rf ".pytest_cache"; fi

.PHONY: clean_dummy_libs
clean_dummy_libs:
	@echo "Cleaning dummy libs from extras"
	@cd src/extras-lib \
		&& if [ -d "dummy_good" ]; then rm --recursive --force "dummy_good"; fi \
		&& if [ -d "dummy_bad" ]; then rm --recursive --force "dummy_bad"; fi


###########################################################################
# CI/CD Specific Targets
###########################################################################
.PHONY: docker-image-setup
docker-image-setup:
	@echo "Setting up Docker image"
	@cd ".gitlab" \
 		&& chmod +x docker-image-setup.bash \
 		&& ./docker-image-setup.bash
