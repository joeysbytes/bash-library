# shellcheck disable=SC2034  # Unused variables check

###########################################################################
# ANSI Initialization
#
# Globals:
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets BASH_FUNCTION_RETURN_CODE
#       0 - Initialization success
###########################################################################
function ansi::_initialize() {
    ansi::off
    BASH_FUNCTION_RETURN_CODE=0
}


###########################################################################
# This sets ANSI global variables.
#
# Globals:
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets BASH_FUNCTION_RETURN_CODE
#       0 - ANSI variables loaded
###########################################################################
function ansi::on() {
    esc="\033";            csi="${esc}[";         resetAnsi="${csi}0m"

    fgBlack="${csi}30m";   bgBlack="${csi}40m";   fgBlack2="${csi}90m";   bgBlack2="${csi}100m"
    fgRed="${csi}31m";     bgRed="${csi}41m";     fgRed2="${csi}91m";     bgRed2="${csi}101m"
    fgGreen="${csi}32m";   bgGreen="${csi}42m";   fgGreen2="${csi}92m";   bgGreen2="${csi}102m"
    fgYellow="${csi}33m";  bgYellow="${csi}43m";  fgYellow2="${csi}93m";  bgYellow2="${csi}103m"
    fgBlue="${csi}34m";    bgBlue="${csi}44m";    fgBlue2="${csi}94m";    bgBlue2="${csi}104m"
    fgMagenta="${csi}35m"; bgMagenta="${csi}45m"; fgMagenta2="${csi}95m"; bgMagenta2="${csi}105m"
    fgCyan="${csi}36m";    bgCyan="${csi}46m";    fgCyan2="${csi}96m";    bgCyan2="${csi}106m"
    fgWhite="${csi}37m";   bgWhite="${csi}47m";   fgWhite2="${csi}97m";   bgWhite2="${csi}107m"
    fgDefault="${csi}39m"; bgDefault="${csi}49m"

    intensity1="${csi}22m"                # medium / normal
    intensity2="${intensity1}${csi}1m"    # bright
    intensity0="${intensity1}${csi}2m"    # dim

    underlineOn="${csi}4m";     underlineOff="${csi}24m"
    italicOn="${csi}3m";        italicOff="${csi}23m"
    blinkOn="${csi}5m";         blinkOff="${csi}25m"
    reverseOn="${csi}7m";       reverseOff="${csi}27m"
    strikethroughOn="${csi}9m"; strikethroughOff="${csi}29m"

    BASH_FUNCTION_RETURN_CODE=0
}


###########################################################################
# This sets ANSI global variables to empty strings
#
# Globals:
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets BASH_FUNCTION_RETURN_CODE
#       0 - ANSI variables loaded
###########################################################################
function ansi::off() {
    esc="";       csi="";       resetAnsi=""

    fgBlack="";   bgBlack="";   fgBlack2="";   bgBlack2=""
    fgRed="";     bgRed="";     fgRed2="";     bgRed2=""
    fgGreen="";   bgGreen="";   fgGreen2="";   bgGreen2=""
    fgYellow="";  bgYellow="";  fgYellow2="";  bgYellow2=""
    fgBlue="";    bgBlue="";    fgBlue2="";    bgBlue2=""
    fgMagenta=""; bgMagenta=""; fgMagenta2=""; bgMagenta2=""
    fgCyan="";    bgCyan="";    fgCyan2="";    bgCyan2=""
    fgWhite="";   bgWhite="";   fgWhite2="";   bgWhite2=""
    fgDefault=""; bgDefault=""

    intensity1=""    # medium / normal
    intensity2=""    # bright
    intensity0=""    # dim

    underlineOn="";     underlineOff=""
    italicOn="";        italicOff=""
    blinkOn="";         blinkOff=""
    reverseOn="";       reverseOff=""
    strikethroughOn=""; strikethroughOff=""

    BASH_FUNCTION_RETURN_CODE=0
}
