#==========================================================================
# This script is the controller for the other bash library functions.
#==========================================================================

# TODO: Help
# TODO: Libs Status
# TODO: Update Docs
# TODO: SDK Docs


###########################################################################
# GLOBAL VARIABLES AND CONSTANTS
###########################################################################

# +------------------+
# | Global Constants |
# +------------------+
declare -A _lib_loader_cfg
declare -A _lib_loader_status
declare -A _lib_loader_paths

_lib_loader_cfg["lib_dir"]="$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)"
_lib_loader_cfg["standard_lib_dir"]="${_lib_loader_cfg['lib_dir']}/standard-lib"
_lib_loader_cfg["extras_lib_dir"]="${_lib_loader_cfg['lib_dir']}/extras-lib"


###########################################################################
# This initialization function is run automatically, and must run before
# any other library functions.
#
# Globals:
#   _lib_loader_cfg
#
# Arguments:
#   None
#
# Outputs:
#   - Return Codes
#         0 - Initialization successful
#       255 - An unspecified error occurred
###########################################################################
function lib_loader::init() {
    local this_library="lib_loader"

    # Load standard / extras libraries if we have not done so before
    if [ "${#_lib_loader_status[@]}" -eq 0 ]; then

        lib_loader::load_available_libs "${_lib_loader_cfg["standard_lib_dir"]}"
        lib_loader::load_available_libs "${_lib_loader_cfg["extras_lib_dir"]}"
    fi
#        _lib_loader_cfg["${this_library}_lib"]="loaded"
#    elif [ ! "${this_library_loaded}" == "loaded" ]; then
#        # should never get to this code
#        echo "ERROR: Could not determine if library loaded: ${this_library}">&2
#        return 255
}


###########################################################################
# Go through the standard-lib and extras-lib directories, and load the
# statuses and paths for the available libraries
#
# Globals:
#   _lib_loader_status
#   _lib_loader_paths
#
# Arguments:
#   1 - Directory of library (absolute path)
#
# Outputs:
#   - Return Codes
#         0 - Success
#       128 - Invalid Usage
#       129 - Directory not found
###########################################################################
function lib_loader::load_available_libs() {
    # validate usage
    if [ $# -ne 1 ]; then
        echo "USAGE: ${FUNCNAME[0]} libraryDirectory">&2
        return 128
    fi
    local lib_dir="${1}"

    # Check that directory exists
    if [ ! -d "${1}" ]; then
        echo "ERROR: ${FUNCNAME[0]}: Library directory not found: ${lib_dir}">&2
        return 129
    fi

    # find directories under the given directory, ignoring hidden ones
    # for l_dir in $(find "${lib_dir}" -maxdepth 1 -type d -not -path '*/.*' -not -path "${lib_dir}"); do
    while IFS= read -r -d '' l_dir; do
        local lib_name
        lib_name="$(basename "${l_dir}")"
        if [ ! -v "_lib_loader_status[${lib_name}]" ]; then
            _lib_loader_status["${lib_name}"]="not loaded"
            _lib_loader_paths["${lib_name}"]="${l_dir}"
        fi
    done < <(find "${lib_dir}" -maxdepth 1 -type d -not -path '*/.*' -not -path "${lib_dir}" -print0)
}


###########################################################################
# Given a library name, check if it is already loaded.
#
# Globals:
#   _lib_loader_cfg
#   _lib_loader_status
#
# Arguments:
#   1) Library name to search for
#
# Outputs:
#   - stdout:
#       loaded     - Library is loaded
#       not loaded - Library is not loaded
#       not found  - Library not found
#   - Return Codes
#         0 - Success
#       128 - Invalid usage
###########################################################################
function lib_loader::lib_status() {
    # validate usage
    if [ $# -ne 1 ]; then
        echo "USAGE: ${FUNCNAME[0]} libraryName">&2
        return 128
    fi
    local library="${1}"

    # search for library in loaded library array
    if [ -v "_lib_loader_status[${library}]" ]; then
        echo -n "${_lib_loader_status[${library}]}"
    else
        echo -n "not found"
    fi
}


###########################################################################
# Load a given library
#
# Globals:
#   _lib_loader_cfg
#
# Arguments:
#   1) Name of library to load
#
# Outputs:
#   - Sources a bash library if found, and calls the "initialization" function
#   - Return Codes
#         0 - Success
#       128 - Invalid usage
#       129 - Library not found
#       253 - Error occurred in library initialization
#       254 - Failed to source in library file (access issues?)
#       255 - An unspecified error occurred
###########################################################################
function lib_loader::load() {
     # validate usage
    if [ $# -ne 1 ]; then
        echo "USAGE: ${FUNCNAME[0]} libraryName">&2
        return 128
    fi
    local lib_to_load="${1}"

    # check if library already loaded
    local lib_loaded_status
    lib_loaded_status="$(lib_loader::is_loaded "${lib_to_load}")"
    if [ "${lib_loaded_status}" == "not loaded" ]; then
        # need to load library
        local lib_file_name
        lib_file_name="${_lib_loader_cfg['standard_lib_dir']}/${lib_to_load}.bash"
        if [ ! -f "${lib_file_name}" ]; then
            lib_file_name="${_lib_loader_cfg['extras_lib_dir']}/${lib_to_load}.bash"
        fi
        if [ -f "${lib_file_name}" ]; then
            # shellcheck source=/dev/null
            if ! source "${lib_file_name}"; then
                echo "ERROR: Failed to source in library file: ${lib_file_name}">&2
                return 254
            fi

            # run the library initialization function
            local init_fn_name="${lib_to_load}::initialize"
            local status_code
            eval "${init_fn_name}"
            status_code=$?
            if [ "${status_code}" -ne 0 ]
            then
                echo "ERROR: Initialization error occurred in library: ${lib_to_load}">&2
                return 253
            else
                _lib_loader_cfg["${lib_to_load}_lib"]="loaded"
            fi
        else
            echo "ERROR: Could not find library to load: ${lib_to_load}">&2
            return 129
        fi
    elif [ ! "${lib_loaded_status}" == "loaded" ]; then
        # should never get to this code
        echo "ERROR: Could not determine if library loaded: ${lib_to_load}">&2
        return 255
    fi
}


###########################################################################
# Initialize the lib_loader when this file is sourced in.
###########################################################################
lib_loader::init
