# shellcheck disable=SC2034  # Unused variables check
# shellcheck disable=SC2154  # ANSI variables externally sourced

# TODO: Documentation (front page, logger)
# TODO: Unit tests not complete

#==========================================================================
# Logging Levels:
#    0 - all
#   10 - trace
#   20 - debug
#   30 - info
#   40 - warn/warning
#   50 - err/error (goes to stderr)
#   60 - fatal (goes to stderr)
#   98 - [unknown] (special, internal used level, goes to stderr)
#   99 - off
#==========================================================================


###########################################################################
# Logger Initialization
#
# Globals:
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets BASH_FUNCTION_RETURN_CODE
#       0 - Initialization success
###########################################################################
function logger::_initialize() {
    lib_loader::load "ansi"
    _LOGGER_LEVEL=30
    _LOGGER_LEVEL_ENABLED=1
    _LOGGER_TIMESTAMP_ENABLED=1
    _LOGGER_TIMESTAMP_FORMAT='%Y-%m-%d %H:%M:%S'
    BASH_FUNCTION_RETURN_CODE=0
}


###########################################################################
# Enable displaying the log level text with the log messages
#
# Globals:
#   _LOGGER_LEVEL_ENABLED
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets _LOGGER_LEVEL_ENABLED to 1
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Log level enabled
#         1 - Log level already enabled
###########################################################################
function logger::enable_log_level() {
    local return_code=0
    if [ "${_LOGGER_LEVEL_ENABLED}" -eq 1 ]; then
        return_code=1
    else
        _LOGGER_LEVEL_ENABLED=1
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Disable displaying the log level text with the log messages
#
# Globals:
#   _LOGGER_LEVEL_ENABLED
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets _LOGGER_LEVEL_ENABLED to 0
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Log level disabled
#         1 - Log level already disabled
###########################################################################
function logger::disable_log_level() {
    local return_code=0
    if [ "${_LOGGER_LEVEL_ENABLED}" -eq 0 ]; then
        return_code=1
    else
        _LOGGER_LEVEL_ENABLED=0
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Set the log level
#
# Globals:
#   _LOGGER_LEVEL
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   1 - Log level to set to.
#       Valid levels: all, trace, debug, info,
#                     warn/warning, err/error, fatal, off
#
# Outputs:
#   - Sets the log level to _LOGGER_LEVEL
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Log level changed
#         1 - Log level already at requested level
#       128 - Invalid number of arguments
#       129 - Invalid log level
###########################################################################
function logger::set_log_level() {
    local return_code=129
    if [ $# -ne 1 ]; then
        return_code=128
    else
        local requested_log_level="${1}"
        local new_log_level=-1
        case "${requested_log_level}" in
            "all")
                new_log_level=0
                ;;
            "trace")
                new_log_level=10
                ;;
            "debug")
                new_log_level=20
                ;;
            "info")
                new_log_level=30
                ;;
            "warn" | "warning")
                new_log_level=40
                ;;
            "err" | "error")
                new_log_level=50
                ;;
            "fatal")
                new_log_level=60
                ;;
            "off")
                new_log_level=99
                ;;
            *)
                return_code=129
                ;;
        esac
        if [ "${new_log_level}" -ge 0 ]; then
            if [ "${new_log_level}" == "${_LOGGER_LEVEL}" ]; then
                return_code=1
            else
                _LOGGER_LEVEL="${new_log_level}"
                return_code=0
            fi
        fi
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Enable displaying the timestamp with the log messages
#
# Globals:
#   _LOGGER_TIMESTAMP_ENABLED
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets _LOGGER_TIMESTAMP_ENABLED to 1
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Timestamp enabled
#         1 - Timestamp already enabled
###########################################################################
function logger::enable_timestamp() {
    local return_code=0
    if [ "${_LOGGER_TIMESTAMP_ENABLED}" -eq 1 ]; then
        return_code=1
    else
        _LOGGER_TIMESTAMP_ENABLED=1
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Disable displaying the timestamp with the log messages
#
# Globals:
#   _LOGGER_TIMESTAMP_ENABLED
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   None
#
# Outputs:
#   - Sets _LOGGER_TIMESTAMP_ENABLED to 0
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Timestamp disabled
#         1 - Timestamp already disabled
###########################################################################
function logger::disable_timestamp() {
    local return_code=0
    if [ "${_LOGGER_TIMESTAMP_ENABLED}" -eq 0 ]; then
        return_code=1
    else
        _LOGGER_TIMESTAMP_ENABLED=0
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Sets the timestamp format as will be interpreted by the date command.
# Note: There are no validations on the format itself.
#
# Globals:
#   _LOGGER_TIMESTAMP_FORMAT
#   BASH_FUNCTION_RETURN_CODE
#
# Arguments:
#   1 - Timestamp string to pass to the date command
#
# Outputs:
#   - Sets _LOGGER_TIMESTAMP_FORMAT to the requested format
#   - Sets BASH_FUNCTION_RETURN_CODE
#         0 - Timestamp format changed
#         1 - Timestamp already at requested format
#       128 - Invalid number of arguments
###########################################################################
function logger::set_timestamp_format() {
    local return_code=0
    if [ $# -ne 1 ]; then
        return_code=128
    else
        local requested_format="${1}"
        if [ "${requested_format}" == "${_LOGGER_TIMESTAMP_FORMAT}" ]; then
            return_code=1
        else
            _LOGGER_TIMESTAMP_FORMAT="${requested_format}"
        fi
    fi
    BASH_FUNCTION_RETURN_CODE="${return_code}"
}


###########################################################################
# Given a log message, build and write the output.
#
# Globals:
#   _LOGGER_LEVEL
#
# Arguments: 1 or 2
#   If no arguments are passed in, an empty info log message will be output.
#   If only 1 argument is passed in, the assumed log level is "info":
#     1 - Log message
#   If 2 arguments are passed in:
#     1 - Log level
#     2 - Log message
#   If more than 3 arguments are passed in, the rest will be ignored.
#
# Outputs:
#   - Writes out the log entry based on the values passed in and the
#       values set in the global variables
#   - Note: based on the values of variables and arguments, no output
#           may be generated.
#   - Note: BASH_FUNCTION_RETURN_CODE is not set
###########################################################################
function log () {
    if [ "${_LOGGER_LEVEL}" -lt 99 ]; then
        # if log level=99, then logging is turned off, do nothing
        local log_msg=""
        local log_level=98
        if [ $# -eq 1 ]; then
            log_msg="${1}"
            log_level=30
        elif [ $# -ge 2 ]; then
            log_msg="${2}"
            case "${1}" in
                "trace")
                    log_level=10
                    ;;
                "debug")
                    log_level=20
                    ;;
                "info")
                    log_level=30
                    ;;
                "warn" | "warning")
                    log_level=40
                    ;;
                "err" | "error")
                    log_level=50
                    ;;
                "fatal")
                    log_level=60
                    ;;
                *)
                    log_level=98
                    ;;
            esac
        fi
        if [ "${log_level}" -ge "${_LOGGER_LEVEL}" ]; then
            logger::_log "${log_level}" "${log_msg}"
        fi
    fi
}


###########################################################################
# Build and write out the log message
#
# Globals:
#   _LOGGER_LEVEL
#   _LOGGER_LEVEL_ENABLED
#   _LOGGER_TIMESTAMP_ENABLED
#   _LOGGER_TIMESTAMP_FORMAT
#
# Arguments:
#   1 - log level value
#   2 - log message
#
# Outputs:
#   - Writes out the log entry based on the values passed in and the
#       values set in the global variables
#   - Note: BASH_FUNCTION_RETURN_CODE is not set
###########################################################################
function logger::_log() {
    local log_level="${1}"
    local log_msg="${2}"
    local line=""
    # Timestamp
    if [ "${_LOGGER_TIMESTAMP_ENABLED}" -eq 1 ]; then
        local timestamp
        timestamp="$(date "+${_LOGGER_TIMESTAMP_FORMAT}")"
        line="${line}${fgCyan}${bgBlack}${italicOn}${timestamp}${italicOff}${fgDefault}${bgDefault}"
        if [ "${_LOGGER_LEVEL_ENABLED}" -eq 1 ]; then
            line="${line} "
        else
            line="${line}: "
        fi
    fi
    # Log Level
    if [ "${_LOGGER_LEVEL_ENABLED}" -eq 1 ]; then
        local level=""
        case "${log_level}" in
            10)
                level="${level}${fgMagenta}${bgBlack}[TRACE"
                ;;
            20)
                level="${level}${fgGreen}${bgBlack}[DEBUG"
                ;;
            30)
                level="${level}${fgWhite}${bgBlack}[INFO "
                ;;
            40)
                level="${level}${fgYellow}${bgBlack}[WARN "
                ;;
            50)
                level="${level}${fgYellow2}${bgRed}[ERROR"
                ;;
            60)
                level="${level}${fgWhite2}${bgMagenta}[FATAL"
                ;;
            *)
                level="${level}${fgBlack}${bgCyan}[?????"
                ;;
        esac
        level="${level}]${fgDefault}${bgDefault}: "
        line="${line}${level}"
    fi
    # Log Message
    line="${line}${log_msg}"
    if [ "${log_level}" -ge 50 ]; then
        # errors or greater go to stderr
        echo -e "${line}">&2
    else
        echo -e "${line}"
    fi
}
